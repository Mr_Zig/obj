var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
// Задание 1
var students = [];
var commonShow = function () {
    console.log('Студент %s набрал %d баллов', this.name, this.point);
    };

function obj(nameStudent, pointStudent) {
    students.push({
        name: nameStudent,
        point: pointStudent,
        show: commonShow,
    });
}
studentsAndPoints.forEach(function(item, i, studentsAndPoints) {
  if (i % 2) {
    obj(studentsAndPoints[i - 1], studentsAndPoints[i]);
  }
});

// Задание 2
obj('Николай Фролов', 0);
obj('Олег Боровой', 0);

// Задание 3
function addPoints(student, plusPoint) {
  students.find(function(i) {
    switch (i.name) {
      case student:
      i.point += plusPoint;
      break;
    }
  });
};
addPoints('Ирина Овчинникова', 30);
addPoints('Александр Малов', 30);
addPoints('Николай Фролов', 10);

// console.log(students);
//Задание 4
console.log('Список студентов:')
students.forEach(function(item) {
  if (item.point >= 30) {
    item.show();
  };
});

// Задание 5
  students.forEach(function(amount) {
  amount.worksAmount=amount.point / 10;
  });
// console.log(students);

// Дополнительное задание
students.findByName = function(findName){
  return students.find(function(item){
        return item.name == findName;
    });
}
console.log(students.findByName('Алексей Левенец'));
